﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Windows.Forms.DataVisualization.Charting;

namespace planificador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Procesos proceso = new Procesos();
        int tiempoProgresoEjecucionProceso = 0;
        //funcion principal para iniciar las consultas y pintar en interfaz la informacion devuelta

        private void Form1_Load(object sender, EventArgs e)
        {
            checkBoxPrioridadCpu.Checked = true;
            textBoxInterrupcion.Enabled = false;
            ListarProcesos();
            ListarProcesosEnEjecucion();
            ListarProcesosTerminados();
            ObtenerCapacidadRecursos();
            ejecucionPrograma.Enabled = true;
            ejecucionPrograma.Start();
            timerObtenerProcesosEjecucion.Enabled = true;
            timerObtenerProcesosEjecucion.Start();
        }

        //evento click en agregar proceso a base de datos con sus respectivas validaciones
        private void BtnIniciarProcesos_Click(object sender, EventArgs e)
        {
            Procesos proceso = new Procesos();
            Random rnd = new Random();
            string status = "enEspera";
            string nombre = inptProceso.Text;
            int[] porcentajes = proceso.ObtenerSumaPorcentajeDeRecursos();//consulta base de datos
            int totalPorcentajeCpu = porcentajes[0];
            int totalPorcentajeRam = porcentajes[1];
            int totalPorcentajeCache = porcentajes[2];
            int tiempoEjecucion = rnd.Next(10, 30);
            bool nombreProcesoExiste;
            int capacidadRecursoCpu = Int32.Parse(textBoxCapacidadCpu.Text),
                capacidadRecursoRam = Int32.Parse(textBoxCapacidadRam.Text),
                capacidadRecursoCache = Int32.Parse(textBoxCapacidadCache.Text);

            if (nombre == "")
            {
                MessageBox.Show("Por favor ingrese un proceso");
            }
            else
            {
                if(totalPorcentajeCpu <= capacidadRecursoCpu && totalPorcentajeRam <= capacidadRecursoRam && totalPorcentajeCache <= capacidadRecursoCache){//si se cumple - generar y almacenar nuevo proceso
                    int porcentajeCpu = rnd.Next(20, 50);
                    int porcentajeRam = rnd.Next(20, 50);
                    int porcentajeCache = rnd.Next(20, 50);
                    nombreProcesoExiste = proceso.ObtenerSiExiteProceso(nombre);
                    if (nombreProcesoExiste)
                    {
                        //MessageBox.Show("proceso existe en base de datos");
                        proceso.EliminarProcesoDb(nombre);
                        proceso.AgregarProceso(nombre, status, porcentajeCpu, porcentajeRam, porcentajeCache, tiempoEjecucion);
                    }
                    else
                    {
                        //MessageBox.Show("proceso no existe en base de datos");
                        proceso.AgregarProceso(nombre, status, porcentajeCpu, porcentajeRam, porcentajeCache, tiempoEjecucion);
                    }
                    ListarProcesos();
                    ListarProcesosEnEjecucion();
                    ListarProcesosTerminados();
                    inptProceso.Text = "";
                }else
                {
                    MessageBox.Show("No se puede agregar un proceso más hasta que se liberen recursos.");
                }
            }   
        }

        //llamar a consultar base de datos para listar los procesos en enEspera y bloqueados para pintar en la interfaz
        public void ListarProcesos()
        {
            Procesos proceso = new Procesos();
            string prioridadRecurso = ObtenerPrioridadRecurso();
            dataGridViewListaProcesos.DataSource = proceso.ListarProceso(prioridadRecurso);
            dataGridViewListaProcesos.Update();
            dataGridViewListaProcesos.Refresh();
        }

        //llamar a consultar base de datos para listar los procesos en ejecucion para pintar en interfaz
        public void ListarProcesosEnEjecucion()
        {
            Procesos proceso = new Procesos();
            string prioridadRecurso = ObtenerPrioridadRecurso();
            dataGridViewProcesosEjecucion.DataSource = proceso.ListarProcesosEjecucion(prioridadRecurso);
            dataGridViewProcesosEjecucion.Update();
            dataGridViewProcesosEjecucion.Refresh();
        }

        //llamar a consultar base de datos para listar los procesos terminados para pintar en la interfaz
        public void ListarProcesosTerminados()
        {
            Procesos proceso = new Procesos();
            string prioridadRecurso = ObtenerPrioridadRecurso();
            dataGridViewProcesosTerminados.DataSource = proceso.ListarProcesosTerminados(prioridadRecurso);
            dataGridViewProcesosTerminados.Update();
            dataGridViewProcesosTerminados.Refresh();
        }

        private void TbxPorcentajeCpu_TextChanged(object sender, EventArgs e)
        {

        }



        private void EjecucionPrograma_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("iniciando lectura");
            ObtenerProcesosEnCola();
        }


        public void ObtenerProcesoEnEjecucion()
        {
            Procesos proceso = new Procesos();
            if(dataGridViewProcesosEjecucion.Rows.Count != 0)
            {
                PintarDatosDelProcesoInterfaz();
                timerObtenerProcesosEjecucion.Stop();
            }
            else
            {
                labelPorcentajeCpu.Text = "0";
                labelPorcentajeRam.Text = "0";
                labelPorcentajeCache.Text = "0";
            }
        }

        /* public void ObtenerProcesoEnEjecucion()
         {
             Procesos proceso = new Procesos();

             if(dataGridViewProcesosEjecucion.Rows.Count == 0)
             {
                 /*ejecucionPrograma.Stop();
                 ObtenerProcesosEnCola();
                 Console.WriteLine("la tabla esta vacia, obtener procesos de la cola");
             }
             else
             {
                 Console.WriteLine("la tabla tiene datos detener timer");
                // ejecucionPrograma.Stop();
                 PintarDatosDelProcesoInterfaz();
             }
         }*/

        /* public void ObtenerProcesosEnCola()
         {
             Procesos proceso = new Procesos();
             int idProceso = 0;
             string estatus = "enEjecucion";
             if(dataGridViewListaProcesos.Rows.Count != 0)
             {
                 Console.WriteLine("intentando tomar valores");
                 idProceso = (int)dataGridViewListaProcesos.CurrentRow.Cells[0].Value;
                 proceso.CambiarEstatusProceso(idProceso, estatus);
                 ListarProcesos();
                 ListarProcesosEnEjecucion();
                 ListarProcesosTerminados();
                 ejecucionPrograma.Enabled = true;
                 ejecucionPrograma.Start();
             }
             else
             {
                 labelPorcentajeCpu.Text = "0";
                 labelPorcentajeRam.Text = "0";
                 labelPorcentajeCache.Text = "0";
                 Console.WriteLine("No hay procesos en Cola");
                 ejecucionPrograma.Start();
             }
         }*/

        public void ObtenerProcesosEnCola()
        {
            Procesos proceso = new Procesos();
            int porcentajeRecursoCpuUsado = Int32.Parse(labelPorcentajeCpu.Text),
                porcentajeRecursoRamUsado = Int32.Parse(labelPorcentajeRam.Text),
                porcentajeRecursoCacheUsado = Int32.Parse(labelPorcentajeCache.Text),
                capacidadRecursoCpu = Int32.Parse(textBoxCapacidadCpu.Text),
                capacidadRecursoRam = Int32.Parse(textBoxCapacidadRam.Text),
                capacidadRecursoCache = Int32.Parse(textBoxCapacidadCache.Text);

            int recursoCpuDisponible = capacidadRecursoCpu - porcentajeRecursoCpuUsado;
            int recursoRamDisponible = capacidadRecursoRam - porcentajeRecursoRamUsado;
            int recursoCacheDisponible = capacidadRecursoCache - porcentajeRecursoCacheUsado;

            if (porcentajeRecursoCpuUsado < capacidadRecursoCpu && porcentajeRecursoRamUsado < capacidadRecursoRam && porcentajeRecursoCacheUsado < capacidadRecursoCache)
            {
                if(dataGridViewListaProcesos.Rows.Count != 0)
                {
                    int totalProcesosEnCola = dataGridViewListaProcesos.Rows.Count;
                    int sumaPorcentajesCpu = 0;
                    int sumaPorcentajesCache = 0;
                    int sumaPorcentajesRam = 0;
                    int[] idProcesos = new int[totalProcesosEnCola];
                    int contador = 0;
                    Console.WriteLine("puedes tomar recursos de la cola");
                    Console.WriteLine(totalProcesosEnCola);
                    foreach (DataGridViewRow row in dataGridViewListaProcesos.Rows) 
                    {
                        sumaPorcentajesCpu += Convert.ToInt32(row.Cells[3].Value);
                        sumaPorcentajesRam += Convert.ToInt32(row.Cells[4].Value);
                        sumaPorcentajesCache += Convert.ToInt32(row.Cells[5].Value);
                        if (sumaPorcentajesCpu <= recursoCpuDisponible && sumaPorcentajesRam <= recursoRamDisponible && sumaPorcentajesCache <= recursoCacheDisponible)
                        {
                            idProcesos[contador] = Convert.ToInt32(row.Cells[0].Value);
                            contador++;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    proceso.CambiarEstatusEjecucion(idProcesos, "enEjecucion");
                    ListarProcesos();
                    ListarProcesosEnEjecucion();
                    ListarProcesosTerminados();
                }
                else
                {
                    Console.WriteLine("no hay datos en la cola");
                }
            }
            else
            {
                Console.WriteLine("Esperar a que se liberen recursos");
            }
        }
        public void PintarDatosDelProcesoInterfaz()
        {
            int idProceso = 0,
                porcentajeCpu = 0,
                porcentajeRam = 0,
                porcentajeCache = 0,
                tiempoEjecucion = 4,
                capacidadRecursoCpu = Int32.Parse(textBoxCapacidadCpu.Text),
                capacidadRecursoRam = Int32.Parse(textBoxCapacidadRam.Text),
                capacidadRecursoCache = Int32.Parse(textBoxCapacidadCache.Text);

            foreach (DataGridViewRow row in dataGridViewProcesosEjecucion.Rows)
            {
                porcentajeCpu += Convert.ToInt32(row.Cells[3].Value);
                porcentajeRam += Convert.ToInt32(row.Cells[4].Value);
                porcentajeCache += Convert.ToInt32(row.Cells[5].Value);
                if (porcentajeCpu <= capacidadRecursoCpu && porcentajeRam <= capacidadRecursoRam && porcentajeCache <= capacidadRecursoCache)
                {

                    labelPorcentajeCpu.Text = porcentajeCpu.ToString();
                    labelPorcentajeRam.Text = porcentajeRam.ToString();
                    labelPorcentajeCache.Text = porcentajeCache.ToString();
                    //tiempoProgresoNumero.Text = tiempoEjecucion.ToString();
                    ejecucionPrograma.Enabled = true;
                    ejecucionPrograma.Start();
                }
                else
                {

                    ejecucionPrograma.Stop();
                }
            }
            idProceso = (int)dataGridViewProcesosEjecucion.CurrentRow.Cells[0].Value;
            tiempoProgresoEjecucionProceso = tiempoEjecucion;
            contadorProgreso.Enabled = true;
            contadorProgreso.Start();
        }

        private void ContadorProgreso_Tick(object sender, EventArgs e)
        {
            Procesos proceso = new Procesos();
            int idProceso = 0;
            string estatus = "";
            string porcentajeCpu = labelPorcentajeCpu.Text;
            string porcentajeRam = labelPorcentajeRam.Text;
            string porcentajeCache = labelPorcentajeCache.Text;
            if (tiempoProgresoEjecucionProceso > 0)
            {
                tiempoProgresoEjecucionProceso--;
                //tiempoProgresoNumero.Text = tiempoProgresoEjecucionProceso.ToString();
                Console.WriteLine(tiempoProgresoEjecucionProceso + " " + porcentajeCpu);
                graficaRecursos.Enabled = true;
                graficaRecursos.Visible = true;
                graficaRecursos.Series["CPU"].Points.AddXY(0, Int32.Parse(porcentajeCpu));
                graficaRecursos.Series["RAM"].Points.AddXY(0, Int32.Parse(porcentajeRam));
                graficaRecursos.Series["CACHE"].Points.AddXY(0, Int32.Parse(porcentajeCache));
            }
            Console.WriteLine(tiempoProgresoEjecucionProceso);
            if (tiempoProgresoEjecucionProceso == 0)
            {
                contadorProgreso.Stop();
                if(dataGridViewProcesosEjecucion.Rows.Count != 0)
                {
                    idProceso = (int)dataGridViewProcesosEjecucion.CurrentRow.Cells[0].Value;
                    estatus = "terminado";
                    proceso.CambiarEstatusProceso(idProceso, estatus);
                    ListarProcesos();
                    ListarProcesosEnEjecucion();
                    ListarProcesosTerminados();
                    timerObtenerProcesosEjecucion.Enabled = true;
                    timerObtenerProcesosEjecucion.Start();
                }
            }
        }

        public void ObtenerCapacidadRecursos()
        {
            Procesos proceso = new Procesos();
            Console.WriteLine("Obteniendo capacidad recursos");
            int[] recursos = proceso.ObtenerCapacidadDeRecursos();
            textBoxCapacidadCpu.Text = recursos[0].ToString();
            textBoxCapacidadRam.Text = recursos[1].ToString();
            textBoxCapacidadCache.Text = recursos[2].ToString();

        }

        private void CheckBoxPrioridadCpu_Click(object sender, EventArgs e)
        {
            Procesos proceso = new Procesos();
            checkBoxPrioridadRam.Checked = false;
            checkBoxPrioridadCache.Checked = false;
            checkBoxPrioridadCpu.Checked = true;
            ListarProcesos();

        }

        private void CheckBoxPrioridadRam_Click(object sender, EventArgs e)
        {
            checkBoxPrioridadRam.Checked = true;
            checkBoxPrioridadCache.Checked = false;
            checkBoxPrioridadCpu.Checked = false;
            ListarProcesos();
        }

        private void CheckBoxPrioridadCache_Click(object sender, EventArgs e)
        {
            checkBoxPrioridadRam.Checked = false;
            checkBoxPrioridadCache.Checked = true;
            checkBoxPrioridadCpu.Checked = false;
            ListarProcesos();
        }

        public string ObtenerPrioridadRecurso()
        {
            string prioridadSistema = "";
            if (checkBoxPrioridadCpu.Checked)
            {
                prioridadSistema = "porcentaje_cpu";
            }else if (checkBoxPrioridadRam.Checked)
            {
                prioridadSistema = "porcentaje_ram";
            }
            else
            {
                prioridadSistema = "porcentaje_cache";
            }
            return prioridadSistema;
        }

        private void TextBoxCapacidadCpu_KeyDown(object sender, KeyEventArgs e)
        {
            Procesos proceso = new Procesos();
            string capacidad = "", recurso = "cpu";
            if (e.KeyCode == Keys.Enter)
            {
                capacidad = textBoxCapacidadCpu.Text;
                proceso.CambiarCapacidadRecurso(recurso, capacidad);
            }
        }

        private void TextBoxCapacidadRam_KeyDown(object sender, KeyEventArgs e)
        {
            Procesos proceso = new Procesos();
            string capacidad = "", recurso = "ram";
            if (e.KeyCode == Keys.Enter)
            {
                capacidad = textBoxCapacidadRam.Text;
                proceso.CambiarCapacidadRecurso(recurso, capacidad);
            }
        }

        private void TextBoxCapacidadCache_KeyDown(object sender, KeyEventArgs e)
        {
            Procesos proceso = new Procesos();
            string capacidad = "", recurso = "cache";
            if (e.KeyCode == Keys.Enter)
            {
                capacidad = textBoxCapacidadCache.Text;
                proceso.CambiarCapacidadRecurso(recurso, capacidad);
            }
        }

        private void TimerObtenerProcesosEjecucion_Tick(object sender, EventArgs e)
        {
            ObtenerProcesoEnEjecucion();
        }

        private void ButtonInterrupcion_Click(object sender, EventArgs e)
        {
            Procesos procesos = new Procesos();
            int idProceso = 0;
            if (dataGridViewProcesosEjecucion.Rows.Count != 0)
            {
                idProceso = (int)dataGridViewProcesosEjecucion.CurrentRow.Cells[0].Value;
                proceso.CambiarEstatusProceso(idProceso,"bloqueado");
                contadorProgreso.Stop();
                timerObtenerProcesosEjecucion.Stop();
                ListarProcesos();
                ListarProcesosEnEjecucion();
                ListarProcesosTerminados();
                contadorProgreso.Enabled = true;
                timerObtenerProcesosEjecucion.Enabled = true;
                contadorProgreso.Start();
                timerObtenerProcesosEjecucion.Start();
            }
            else
            {
                MessageBox.Show("No hay procesos en ejecucion");
            }
        }
    }
}
