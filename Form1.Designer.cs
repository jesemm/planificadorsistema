﻿namespace planificador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.contenedorGrafica = new System.Windows.Forms.Panel();
            this.graficaRecursos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelRam = new System.Windows.Forms.Label();
            this.iconRam = new System.Windows.Forms.PictureBox();
            this.contenedorRam = new System.Windows.Forms.Panel();
            this.textBoxCapacidadRam = new System.Windows.Forms.TextBox();
            this.labelCapacidadRam = new System.Windows.Forms.Label();
            this.checkBoxPrioridadRam = new System.Windows.Forms.CheckBox();
            this.labelPorcentajeRam = new System.Windows.Forms.Label();
            this.iconoCache = new System.Windows.Forms.PictureBox();
            this.labelCache = new System.Windows.Forms.Label();
            this.contenedorCache = new System.Windows.Forms.Panel();
            this.textBoxCapacidadCache = new System.Windows.Forms.TextBox();
            this.labelCapacidadCache = new System.Windows.Forms.Label();
            this.checkBoxPrioridadCache = new System.Windows.Forms.CheckBox();
            this.labelPorcentajeCache = new System.Windows.Forms.Label();
            this.labelTitleCache2 = new System.Windows.Forms.Label();
            this.inptProceso = new System.Windows.Forms.TextBox();
            this.btnIniciarProcesos = new System.Windows.Forms.Button();
            this.labelProcesoInpt = new System.Windows.Forms.Label();
            this.contenedorForm = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.iconoCpu = new System.Windows.Forms.PictureBox();
            this.labelCpu = new System.Windows.Forms.Label();
            this.contedorCpu = new System.Windows.Forms.Panel();
            this.textBoxCapacidadCpu = new System.Windows.Forms.TextBox();
            this.checkBoxPrioridadCpu = new System.Windows.Forms.CheckBox();
            this.labelCapacidadCpu = new System.Windows.Forms.Label();
            this.labelPorcentajeCpu = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewProcesosEjecucion = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewProcesosTerminados = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewListaProcesos = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBoxInterrupcion = new System.Windows.Forms.TextBox();
            this.buttonInterrupcion = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tiempoProgresoNumero = new System.Windows.Forms.Label();
            this.ejecucionPrograma = new System.Windows.Forms.Timer(this.components);
            this.contadorProgreso = new System.Windows.Forms.Timer(this.components);
            this.timerObtenerProcesosEjecucion = new System.Windows.Forms.Timer(this.components);
            this.contenedorGrafica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graficaRecursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconRam)).BeginInit();
            this.contenedorRam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconoCache)).BeginInit();
            this.contenedorCache.SuspendLayout();
            this.contenedorForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconoCpu)).BeginInit();
            this.contedorCpu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesosEjecucion)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesosTerminados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListaProcesos)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedorGrafica
            // 
            this.contenedorGrafica.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.contenedorGrafica.Controls.Add(this.graficaRecursos);
            this.contenedorGrafica.Location = new System.Drawing.Point(232, 26);
            this.contenedorGrafica.Name = "contenedorGrafica";
            this.contenedorGrafica.Size = new System.Drawing.Size(659, 266);
            this.contenedorGrafica.TabIndex = 0;
            // 
            // graficaRecursos
            // 
            chartArea2.Name = "ChartArea1";
            this.graficaRecursos.ChartAreas.Add(chartArea2);
            this.graficaRecursos.DataSource = this.graficaRecursos.Series;
            legend2.Name = "Legend1";
            this.graficaRecursos.Legends.Add(legend2);
            this.graficaRecursos.Location = new System.Drawing.Point(48, 14);
            this.graficaRecursos.Name = "graficaRecursos";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.LabelBorderWidth = 3;
            series4.Legend = "Legend1";
            series4.Name = "CPU";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.LabelBorderWidth = 3;
            series5.Legend = "Legend1";
            series5.Name = "RAM";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.LabelBorderWidth = 3;
            series6.Legend = "Legend1";
            series6.Name = "CACHE";
            this.graficaRecursos.Series.Add(series4);
            this.graficaRecursos.Series.Add(series5);
            this.graficaRecursos.Series.Add(series6);
            this.graficaRecursos.Size = new System.Drawing.Size(590, 223);
            this.graficaRecursos.TabIndex = 0;
            this.graficaRecursos.Text = "chart1";
            this.graficaRecursos.Visible = false;
            // 
            // labelRam
            // 
            this.labelRam.AutoSize = true;
            this.labelRam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelRam.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelRam.Location = new System.Drawing.Point(76, 12);
            this.labelRam.Name = "labelRam";
            this.labelRam.Size = new System.Drawing.Size(48, 20);
            this.labelRam.TabIndex = 1;
            this.labelRam.Text = "RAM";
            // 
            // iconRam
            // 
            this.iconRam.Image = ((System.Drawing.Image)(resources.GetObject("iconRam.Image")));
            this.iconRam.Location = new System.Drawing.Point(10, 6);
            this.iconRam.Name = "iconRam";
            this.iconRam.Size = new System.Drawing.Size(61, 70);
            this.iconRam.TabIndex = 0;
            this.iconRam.TabStop = false;
            // 
            // contenedorRam
            // 
            this.contenedorRam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.contenedorRam.Controls.Add(this.textBoxCapacidadRam);
            this.contenedorRam.Controls.Add(this.labelCapacidadRam);
            this.contenedorRam.Controls.Add(this.checkBoxPrioridadRam);
            this.contenedorRam.Controls.Add(this.labelPorcentajeRam);
            this.contenedorRam.Controls.Add(this.iconRam);
            this.contenedorRam.Controls.Add(this.labelRam);
            this.contenedorRam.Location = new System.Drawing.Point(12, 116);
            this.contenedorRam.Name = "contenedorRam";
            this.contenedorRam.Size = new System.Drawing.Size(211, 84);
            this.contenedorRam.TabIndex = 2;
            // 
            // textBoxCapacidadRam
            // 
            this.textBoxCapacidadRam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.textBoxCapacidadRam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxCapacidadRam.ForeColor = System.Drawing.SystemColors.Menu;
            this.textBoxCapacidadRam.Location = new System.Drawing.Point(163, 44);
            this.textBoxCapacidadRam.Name = "textBoxCapacidadRam";
            this.textBoxCapacidadRam.Size = new System.Drawing.Size(45, 26);
            this.textBoxCapacidadRam.TabIndex = 4;
            this.textBoxCapacidadRam.Text = "0";
            this.textBoxCapacidadRam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxCapacidadRam_KeyDown);
            // 
            // labelCapacidadRam
            // 
            this.labelCapacidadRam.AutoSize = true;
            this.labelCapacidadRam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelCapacidadRam.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCapacidadRam.Location = new System.Drawing.Point(143, 47);
            this.labelCapacidadRam.Name = "labelCapacidadRam";
            this.labelCapacidadRam.Size = new System.Drawing.Size(14, 20);
            this.labelCapacidadRam.TabIndex = 3;
            this.labelCapacidadRam.Text = "/";
            // 
            // checkBoxPrioridadRam
            // 
            this.checkBoxPrioridadRam.AutoSize = true;
            this.checkBoxPrioridadRam.Location = new System.Drawing.Point(187, 13);
            this.checkBoxPrioridadRam.Name = "checkBoxPrioridadRam";
            this.checkBoxPrioridadRam.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPrioridadRam.TabIndex = 3;
            this.checkBoxPrioridadRam.UseVisualStyleBackColor = true;
            this.checkBoxPrioridadRam.Click += new System.EventHandler(this.CheckBoxPrioridadRam_Click);
            // 
            // labelPorcentajeRam
            // 
            this.labelPorcentajeRam.AutoSize = true;
            this.labelPorcentajeRam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelPorcentajeRam.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPorcentajeRam.Location = new System.Drawing.Point(81, 47);
            this.labelPorcentajeRam.Name = "labelPorcentajeRam";
            this.labelPorcentajeRam.Size = new System.Drawing.Size(18, 20);
            this.labelPorcentajeRam.TabIndex = 1;
            this.labelPorcentajeRam.Text = "0";
            // 
            // iconoCache
            // 
            this.iconoCache.Image = ((System.Drawing.Image)(resources.GetObject("iconoCache.Image")));
            this.iconoCache.Location = new System.Drawing.Point(8, 7);
            this.iconoCache.Name = "iconoCache";
            this.iconoCache.Size = new System.Drawing.Size(56, 62);
            this.iconoCache.TabIndex = 0;
            this.iconoCache.TabStop = false;
            // 
            // labelCache
            // 
            this.labelCache.AutoSize = true;
            this.labelCache.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelCache.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelCache.Location = new System.Drawing.Point(63, 7);
            this.labelCache.Name = "labelCache";
            this.labelCache.Size = new System.Drawing.Size(77, 20);
            this.labelCache.TabIndex = 1;
            this.labelCache.Text = "Memoria";
            // 
            // contenedorCache
            // 
            this.contenedorCache.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.contenedorCache.Controls.Add(this.textBoxCapacidadCache);
            this.contenedorCache.Controls.Add(this.labelCapacidadCache);
            this.contenedorCache.Controls.Add(this.checkBoxPrioridadCache);
            this.contenedorCache.Controls.Add(this.labelPorcentajeCache);
            this.contenedorCache.Controls.Add(this.labelTitleCache2);
            this.contenedorCache.Controls.Add(this.labelCache);
            this.contenedorCache.Controls.Add(this.iconoCache);
            this.contenedorCache.Location = new System.Drawing.Point(12, 206);
            this.contenedorCache.Name = "contenedorCache";
            this.contenedorCache.Size = new System.Drawing.Size(211, 86);
            this.contenedorCache.TabIndex = 3;
            // 
            // textBoxCapacidadCache
            // 
            this.textBoxCapacidadCache.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.textBoxCapacidadCache.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxCapacidadCache.ForeColor = System.Drawing.SystemColors.Menu;
            this.textBoxCapacidadCache.Location = new System.Drawing.Point(163, 50);
            this.textBoxCapacidadCache.Name = "textBoxCapacidadCache";
            this.textBoxCapacidadCache.Size = new System.Drawing.Size(45, 26);
            this.textBoxCapacidadCache.TabIndex = 5;
            this.textBoxCapacidadCache.Text = "0";
            this.textBoxCapacidadCache.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxCapacidadCache_KeyDown);
            // 
            // labelCapacidadCache
            // 
            this.labelCapacidadCache.AutoSize = true;
            this.labelCapacidadCache.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelCapacidadCache.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCapacidadCache.Location = new System.Drawing.Point(143, 53);
            this.labelCapacidadCache.Name = "labelCapacidadCache";
            this.labelCapacidadCache.Size = new System.Drawing.Size(14, 20);
            this.labelCapacidadCache.TabIndex = 4;
            this.labelCapacidadCache.Text = "/";
            // 
            // checkBoxPrioridadCache
            // 
            this.checkBoxPrioridadCache.AutoSize = true;
            this.checkBoxPrioridadCache.Location = new System.Drawing.Point(189, 12);
            this.checkBoxPrioridadCache.Name = "checkBoxPrioridadCache";
            this.checkBoxPrioridadCache.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPrioridadCache.TabIndex = 4;
            this.checkBoxPrioridadCache.UseVisualStyleBackColor = true;
            this.checkBoxPrioridadCache.Click += new System.EventHandler(this.CheckBoxPrioridadCache_Click);
            // 
            // labelPorcentajeCache
            // 
            this.labelPorcentajeCache.AutoSize = true;
            this.labelPorcentajeCache.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelPorcentajeCache.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPorcentajeCache.Location = new System.Drawing.Point(73, 53);
            this.labelPorcentajeCache.Name = "labelPorcentajeCache";
            this.labelPorcentajeCache.Size = new System.Drawing.Size(18, 20);
            this.labelPorcentajeCache.TabIndex = 2;
            this.labelPorcentajeCache.Text = "0";
            // 
            // labelTitleCache2
            // 
            this.labelTitleCache2.AutoSize = true;
            this.labelTitleCache2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelTitleCache2.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelTitleCache2.Location = new System.Drawing.Point(63, 24);
            this.labelTitleCache2.Name = "labelTitleCache2";
            this.labelTitleCache2.Size = new System.Drawing.Size(57, 20);
            this.labelTitleCache2.TabIndex = 3;
            this.labelTitleCache2.Text = "cache";
            // 
            // inptProceso
            // 
            this.inptProceso.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.inptProceso.Location = new System.Drawing.Point(25, 49);
            this.inptProceso.Name = "inptProceso";
            this.inptProceso.Size = new System.Drawing.Size(209, 29);
            this.inptProceso.TabIndex = 5;
            this.inptProceso.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnIniciarProcesos
            // 
            this.btnIniciarProcesos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.btnIniciarProcesos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnIniciarProcesos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarProcesos.FlatAppearance.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.btnIniciarProcesos.FlatAppearance.BorderSize = 2;
            this.btnIniciarProcesos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.btnIniciarProcesos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarProcesos.Location = new System.Drawing.Point(25, 94);
            this.btnIniciarProcesos.Name = "btnIniciarProcesos";
            this.btnIniciarProcesos.Size = new System.Drawing.Size(200, 40);
            this.btnIniciarProcesos.TabIndex = 4;
            this.btnIniciarProcesos.Text = "Agregar";
            this.btnIniciarProcesos.UseVisualStyleBackColor = false;
            this.btnIniciarProcesos.Click += new System.EventHandler(this.BtnIniciarProcesos_Click);
            // 
            // labelProcesoInpt
            // 
            this.labelProcesoInpt.AutoSize = true;
            this.labelProcesoInpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProcesoInpt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelProcesoInpt.Location = new System.Drawing.Point(23, 14);
            this.labelProcesoInpt.Name = "labelProcesoInpt";
            this.labelProcesoInpt.Size = new System.Drawing.Size(211, 20);
            this.labelProcesoInpt.TabIndex = 6;
            this.labelProcesoInpt.Text = "Ingresa nombre del proceso ";
            // 
            // contenedorForm
            // 
            this.contenedorForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.contenedorForm.Controls.Add(this.labelProcesoInpt);
            this.contenedorForm.Controls.Add(this.btnIniciarProcesos);
            this.contenedorForm.Controls.Add(this.inptProceso);
            this.contenedorForm.Location = new System.Drawing.Point(12, 309);
            this.contenedorForm.Name = "contenedorForm";
            this.contenedorForm.Size = new System.Drawing.Size(251, 153);
            this.contenedorForm.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Location = new System.Drawing.Point(211, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Cola de procesos";
            // 
            // iconoCpu
            // 
            this.iconoCpu.Image = ((System.Drawing.Image)(resources.GetObject("iconoCpu.Image")));
            this.iconoCpu.Location = new System.Drawing.Point(10, 8);
            this.iconoCpu.Name = "iconoCpu";
            this.iconoCpu.Size = new System.Drawing.Size(61, 68);
            this.iconoCpu.TabIndex = 0;
            this.iconoCpu.TabStop = false;
            // 
            // labelCpu
            // 
            this.labelCpu.AutoSize = true;
            this.labelCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelCpu.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelCpu.Location = new System.Drawing.Point(81, 13);
            this.labelCpu.Name = "labelCpu";
            this.labelCpu.Size = new System.Drawing.Size(45, 20);
            this.labelCpu.TabIndex = 0;
            this.labelCpu.Text = "CPU";
            // 
            // contedorCpu
            // 
            this.contedorCpu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.contedorCpu.Controls.Add(this.textBoxCapacidadCpu);
            this.contedorCpu.Controls.Add(this.checkBoxPrioridadCpu);
            this.contedorCpu.Controls.Add(this.labelCapacidadCpu);
            this.contedorCpu.Controls.Add(this.labelPorcentajeCpu);
            this.contedorCpu.Controls.Add(this.labelCpu);
            this.contedorCpu.Controls.Add(this.iconoCpu);
            this.contedorCpu.Location = new System.Drawing.Point(12, 23);
            this.contedorCpu.Name = "contedorCpu";
            this.contedorCpu.Size = new System.Drawing.Size(214, 87);
            this.contedorCpu.TabIndex = 1;
            // 
            // textBoxCapacidadCpu
            // 
            this.textBoxCapacidadCpu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.textBoxCapacidadCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxCapacidadCpu.ForeColor = System.Drawing.SystemColors.Menu;
            this.textBoxCapacidadCpu.Location = new System.Drawing.Point(167, 44);
            this.textBoxCapacidadCpu.Name = "textBoxCapacidadCpu";
            this.textBoxCapacidadCpu.Size = new System.Drawing.Size(44, 26);
            this.textBoxCapacidadCpu.TabIndex = 3;
            this.textBoxCapacidadCpu.Text = "0";
            this.textBoxCapacidadCpu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxCapacidadCpu_KeyDown);
            // 
            // checkBoxPrioridadCpu
            // 
            this.checkBoxPrioridadCpu.AutoSize = true;
            this.checkBoxPrioridadCpu.Location = new System.Drawing.Point(189, 13);
            this.checkBoxPrioridadCpu.Name = "checkBoxPrioridadCpu";
            this.checkBoxPrioridadCpu.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPrioridadCpu.TabIndex = 2;
            this.checkBoxPrioridadCpu.UseVisualStyleBackColor = true;
            this.checkBoxPrioridadCpu.Click += new System.EventHandler(this.CheckBoxPrioridadCpu_Click);
            // 
            // labelCapacidadCpu
            // 
            this.labelCapacidadCpu.AutoSize = true;
            this.labelCapacidadCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelCapacidadCpu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCapacidadCpu.Location = new System.Drawing.Point(144, 47);
            this.labelCapacidadCpu.Name = "labelCapacidadCpu";
            this.labelCapacidadCpu.Size = new System.Drawing.Size(14, 20);
            this.labelCapacidadCpu.TabIndex = 1;
            this.labelCapacidadCpu.Text = "/";
            // 
            // labelPorcentajeCpu
            // 
            this.labelPorcentajeCpu.AutoSize = true;
            this.labelPorcentajeCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelPorcentajeCpu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPorcentajeCpu.Location = new System.Drawing.Point(80, 46);
            this.labelPorcentajeCpu.Name = "labelPorcentajeCpu";
            this.labelPorcentajeCpu.Size = new System.Drawing.Size(18, 20);
            this.labelPorcentajeCpu.TabIndex = 0;
            this.labelPorcentajeCpu.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.panel1.Controls.Add(this.dataGridViewProcesosEjecucion);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(897, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 295);
            this.panel1.TabIndex = 12;
            // 
            // dataGridViewProcesosEjecucion
            // 
            this.dataGridViewProcesosEjecucion.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.dataGridViewProcesosEjecucion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProcesosEjecucion.Location = new System.Drawing.Point(18, 29);
            this.dataGridViewProcesosEjecucion.Name = "dataGridViewProcesosEjecucion";
            this.dataGridViewProcesosEjecucion.Size = new System.Drawing.Size(240, 263);
            this.dataGridViewProcesosEjecucion.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label3.Location = new System.Drawing.Point(41, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Procesos en ejecucion";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.dataGridViewProcesosTerminados);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(897, 338);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(272, 306);
            this.panel2.TabIndex = 13;
            // 
            // dataGridViewProcesosTerminados
            // 
            this.dataGridViewProcesosTerminados.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.dataGridViewProcesosTerminados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProcesosTerminados.Location = new System.Drawing.Point(18, 27);
            this.dataGridViewProcesosTerminados.Name = "dataGridViewProcesosTerminados";
            this.dataGridViewProcesosTerminados.Size = new System.Drawing.Size(240, 276);
            this.dataGridViewProcesosTerminados.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label2.Location = new System.Drawing.Point(45, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Procesos terminados";
            // 
            // dataGridViewListaProcesos
            // 
            this.dataGridViewListaProcesos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.dataGridViewListaProcesos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListaProcesos.Location = new System.Drawing.Point(34, 56);
            this.dataGridViewListaProcesos.Name = "dataGridViewListaProcesos";
            this.dataGridViewListaProcesos.Size = new System.Drawing.Size(531, 279);
            this.dataGridViewListaProcesos.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.dataGridViewListaProcesos);
            this.panel3.Location = new System.Drawing.Point(280, 309);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(597, 335);
            this.panel3.TabIndex = 14;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.panel4.Controls.Add(this.textBoxInterrupcion);
            this.panel4.Controls.Add(this.buttonInterrupcion);
            this.panel4.Location = new System.Drawing.Point(12, 468);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(251, 104);
            this.panel4.TabIndex = 8;
            // 
            // textBoxInterrupcion
            // 
            this.textBoxInterrupcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.textBoxInterrupcion.Location = new System.Drawing.Point(25, 22);
            this.textBoxInterrupcion.Name = "textBoxInterrupcion";
            this.textBoxInterrupcion.Size = new System.Drawing.Size(209, 29);
            this.textBoxInterrupcion.TabIndex = 7;
            this.textBoxInterrupcion.Text = "21H";
            this.textBoxInterrupcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonInterrupcion
            // 
            this.buttonInterrupcion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.buttonInterrupcion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonInterrupcion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonInterrupcion.FlatAppearance.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.buttonInterrupcion.FlatAppearance.BorderSize = 2;
            this.buttonInterrupcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonInterrupcion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInterrupcion.Location = new System.Drawing.Point(26, 57);
            this.buttonInterrupcion.Name = "buttonInterrupcion";
            this.buttonInterrupcion.Size = new System.Drawing.Size(200, 32);
            this.buttonInterrupcion.TabIndex = 4;
            this.buttonInterrupcion.Text = "Interrupción";
            this.buttonInterrupcion.UseVisualStyleBackColor = false;
            this.buttonInterrupcion.Click += new System.EventHandler(this.ButtonInterrupcion_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(41)))), ((int)(((byte)(54)))));
            this.panel5.Controls.Add(this.tiempoProgresoNumero);
            this.panel5.Location = new System.Drawing.Point(12, 581);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(251, 63);
            this.panel5.TabIndex = 9;
            // 
            // tiempoProgresoNumero
            // 
            this.tiempoProgresoNumero.AutoSize = true;
            this.tiempoProgresoNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.tiempoProgresoNumero.ForeColor = System.Drawing.Color.Gainsboro;
            this.tiempoProgresoNumero.Location = new System.Drawing.Point(107, 17);
            this.tiempoProgresoNumero.Name = "tiempoProgresoNumero";
            this.tiempoProgresoNumero.Size = new System.Drawing.Size(0, 31);
            this.tiempoProgresoNumero.TabIndex = 13;
            // 
            // ejecucionPrograma
            // 
            this.ejecucionPrograma.Interval = 2000;
            this.ejecucionPrograma.Tick += new System.EventHandler(this.EjecucionPrograma_Tick);
            // 
            // contadorProgreso
            // 
            this.contadorProgreso.Interval = 1000;
            this.contadorProgreso.Tick += new System.EventHandler(this.ContadorProgreso_Tick);
            // 
            // timerObtenerProcesosEjecucion
            // 
            this.timerObtenerProcesosEjecucion.Interval = 1000;
            this.timerObtenerProcesosEjecucion.Tick += new System.EventHandler(this.TimerObtenerProcesosEjecucion_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1193, 656);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.contenedorForm);
            this.Controls.Add(this.contenedorCache);
            this.Controls.Add(this.contenedorRam);
            this.Controls.Add(this.contedorCpu);
            this.Controls.Add(this.contenedorGrafica);
            this.Name = "Form1";
            this.Text = "-";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contenedorGrafica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graficaRecursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconRam)).EndInit();
            this.contenedorRam.ResumeLayout(false);
            this.contenedorRam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconoCache)).EndInit();
            this.contenedorCache.ResumeLayout(false);
            this.contenedorCache.PerformLayout();
            this.contenedorForm.ResumeLayout(false);
            this.contenedorForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconoCpu)).EndInit();
            this.contedorCpu.ResumeLayout(false);
            this.contedorCpu.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesosEjecucion)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesosTerminados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListaProcesos)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelRam;
        private System.Windows.Forms.PictureBox iconRam;
        private System.Windows.Forms.Panel contenedorRam;
        private System.Windows.Forms.PictureBox iconoCache;
        private System.Windows.Forms.Label labelCache;
        private System.Windows.Forms.Panel contenedorCache;
        private System.Windows.Forms.Label labelTitleCache2;
        private System.Windows.Forms.TextBox inptProceso;
        private System.Windows.Forms.Button btnIniciarProcesos;
        private System.Windows.Forms.Label labelProcesoInpt;
        private System.Windows.Forms.Panel contenedorForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox iconoCpu;
        private System.Windows.Forms.Label labelCpu;
        private System.Windows.Forms.Panel contedorCpu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewListaProcesos;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridViewProcesosEjecucion;
        private System.Windows.Forms.DataGridView dataGridViewProcesosTerminados;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonInterrupcion;
        private System.Windows.Forms.Label tiempoProgresoNumero;
        private System.Windows.Forms.Panel contenedorGrafica;
        private System.Windows.Forms.Timer ejecucionPrograma;
        private System.Windows.Forms.Label labelPorcentajeRam;
        private System.Windows.Forms.Label labelPorcentajeCache;
        private System.Windows.Forms.Label labelPorcentajeCpu;
        private System.Windows.Forms.DataVisualization.Charting.Chart graficaRecursos;
        private System.Windows.Forms.Timer contadorProgreso;
        private System.Windows.Forms.CheckBox checkBoxPrioridadCpu;
        private System.Windows.Forms.Label labelCapacidadCpu;
        private System.Windows.Forms.Label labelCapacidadRam;
        private System.Windows.Forms.CheckBox checkBoxPrioridadRam;
        private System.Windows.Forms.CheckBox checkBoxPrioridadCache;
        private System.Windows.Forms.Label labelCapacidadCache;
        private System.Windows.Forms.TextBox textBoxCapacidadCpu;
        private System.Windows.Forms.TextBox textBoxCapacidadRam;
        private System.Windows.Forms.TextBox textBoxCapacidadCache;
        private System.Windows.Forms.Timer timerObtenerProcesosEjecucion;
        private System.Windows.Forms.TextBox textBoxInterrupcion;
    }
}

