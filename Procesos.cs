﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace planificador
{
    class Procesos
    {
        private string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=planificador;";

        //consulta a base de datos para agregar proceso
        public void AgregarProceso(string nombre, string status, int porcentajeCpu, int porcentajeRam, int porcentajeCache, int tiempoEjecucion)
        {
            string query = "INSERT INTO procesos(id, nombre, status, porcentaje_cpu, porcentaje_ram, porcentaje_cache, tiempo_ejecucion) VALUES (NULL, '" + nombre + "', '" + status + "', '" + porcentajeCpu + "', '" + porcentajeRam + "', '" + porcentajeCache + "', '" + tiempoEjecucion + "')";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            try
            {
                databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier error
                Console.WriteLine(ex.ToString());
            }
        }

        //consulta a base de datos para lsitar procesos con estatus enEspera o bloqueados
        public DataTable ListarProceso(string recursoPrioridad = "porcentaje_cpu")
        {
            DataTable Tabla = new DataTable();
            string query = "SELECT id, nombre, status, porcentaje_cpu, porcentaje_ram, porcentaje_cache FROM procesos WHERE status='enEspera' OR status='bloqueado' ORDER BY " + recursoPrioridad + " ASC";
            //string query = "SELECT id, nombre, status, porcentaje_cpu, tiempo_ejecucion FROM procesos WHERE status='enEspera' OR status='bloqueado'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                // Si se encontraron datos
                if (reader.HasRows)
                {
                    Tabla.Load(reader);
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Tabla;
        }

        //consulta a base de datos para listar procesos con estatus enEjecucion
        public DataTable ListarProcesosEjecucion(string recursoPrioridad)
        {
            DataTable Tabla = new DataTable();
            string query = "SELECT id, nombre, status, porcentaje_cpu, porcentaje_ram, porcentaje_cache FROM procesos WHERE status='enEjecucion' ORDER BY " + recursoPrioridad + " ASC";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                // Si se encontraron datos
                if (reader.HasRows)
                {
                    Tabla.Load(reader);
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Tabla;
        }

        //consulta a base de datos para listar procesos con estatus terminado
        public DataTable ListarProcesosTerminados(string recursoPrioridad)
        {
            DataTable Tabla = new DataTable();
            string query = "SELECT id, nombre FROM procesos WHERE status='terminado' ORDER BY " + recursoPrioridad + " ASC";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                // Si se encontraron datos
                if (reader.HasRows)
                {
                    Tabla.Load(reader);
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Tabla;
        }

        //consulta a base de datos para  obtener la suma de los porcentajes de cada resurso: CPU, RAM y CACHE
        public int[] ObtenerSumaPorcentajeDeRecursos()
        {
            int totalPorcentajeCpu = 0,
                totalPorcentajeRam = 0,
                totalPorcentajeCache = 0;

            string query = "SELECT porcentaje_cpu, porcentaje_ram, porcentaje_cache FROM procesos WHERE status='enEspera' OR status='bloqueado'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                // Si se encontraron datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        totalPorcentajeCpu += (int)reader.GetValue(0);
                        totalPorcentajeRam += (int)reader.GetValue(1);
                        totalPorcentajeCache += (int)reader.GetValue(2);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return new[] { totalPorcentajeCpu, totalPorcentajeRam, totalPorcentajeCache};
        }

        //cambiar el estatus del prroceso a la base de datos
        public void CambiarEstatusProceso(int id, string estatus)
        {
            string query = "UPDATE procesos SET status='"+estatus+"' WHERE id='"+id+"'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public bool ObtenerSiExiteProceso(string nombreProceso)
        {
            bool procesoExiste = false;
            string query = "SELECT nombre FROM procesos WHERE nombre='"+nombreProceso+"'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    procesoExiste = true;
                }
                else
                {
                    procesoExiste = false;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return procesoExiste;
        }

        public void EliminarProcesoDb(string nombreProceso)
        {
            string query = "DELETE FROM procesos WHERE nombre='" + nombreProceso + "'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public int[] ObtenerCapacidadDeRecursos()
        {
            int[] capacidadRecursos = new int[3];
            string query = "SELECT capacidad FROM recursos_sistema";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    int i = 0;
                    while (reader.Read())
                    {
                        capacidadRecursos[i] = (int)reader.GetValue(0);
                        Console.WriteLine(capacidadRecursos[i++]);
                    }
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return capacidadRecursos;
        }
        
        public void CambiarCapacidadRecurso(string recurso, string capacidad)
        {
            string query = "UPDATE recursos_sistema SET capacidad='" + capacidad + "' WHERE nombre='" + recurso + "'";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void CambiarEstatusEjecucion(int[] id, string estatus)
        {
            for (int i = 0; i < id.Length; i++)
            {
                if(id[i] != 0)
                {
                    string query = "UPDATE procesos SET status='" + estatus + "' WHERE id='" + id[i] + "'";
                    MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                    MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                    commandDatabase.CommandTimeout = 60;
                    MySqlDataReader reader;
                    try
                    {
                        databaseConnection.Open();
                        reader = commandDatabase.ExecuteReader();
                        databaseConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        }
    }
}
